package com.company;
import java.util.Random;
import java.util.HashMap;
import java.util.TreeMap;

public class Main {
    public static void main(String[] args) {
        TreeMap<Integer, HashMap<String, Double>> result = new TreeMap<Integer, HashMap<String, Double>>();

	    for (int i = 100; i<=200000; i = i < 100000? i * 10 : i * 2) {
            result.put(i, benchmark(i));
        }

	    System.out.println(String.format("Input size | Array time | LinkedList time"));

	    for(int item:result.keySet()){
	        System.out.println(String.format("%9d%14.4f%14.4f", item, result.get(item).get("array"), result.get(item).get("llist")));
        }
    }

    private static HashMap<String, Double> benchmark(int sequenceLenght) {
        int sequence[] = generateSequence(sequenceLenght);
        int indexes[] = generateIndexes(sequenceLenght);

        System.out.println("Random sequences generated");

        Array arr = new Array();

        System.gc();
        System.gc();
        System.gc();

        long time1 = System.nanoTime();

        for(int i = 0; i < sequence.length; i++){
            arr.add(sequence[i]);
        }

        for(int i = 0; i < indexes.length; i++){
            arr.remove(indexes[i]);
        }

        long time2 = System.nanoTime();

        System.out.println("Array test finished");

        List list = new List();

        System.gc();
        System.gc();
        System.gc();

        long time3 = System.nanoTime();

        for(int i = 0; i < sequence.length; i++){
            list.add(sequence[i]);
        }

        for(int i = 0; i < indexes.length; i++){
            list.remove(indexes[i]);
        }

        long time4 = System.nanoTime();

        System.out.println("Linked list test finished");

        HashMap<String, Double> measures = new HashMap<String, Double>();
        measures.put("array",(time2 - time1) / 1e9);
        measures.put("llist",(time4 - time3) / 1e9);

        System.out.println(String.format("n = %d test finshed", sequenceLenght));

        return measures;
    }

    public  static int[] generateSequence(int sequenceLenght){
        return new Random().ints(sequenceLenght, 0, sequenceLenght).toArray();
    }

    public static int[] generateIndexes(int sequenceLenght){
        Random randomGen = new Random();

        int indexes[] = new int[sequenceLenght];

        for(int i = sequenceLenght; i > 0; i--){
            indexes[sequenceLenght - i] = randomGen.nextInt(i);
        }

        return indexes;
    }
}
