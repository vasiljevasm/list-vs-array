package com.company;
import java.util.ArrayList;

public class Array {
    public ArrayList<Integer> arr;

    public Array(){
        arr = new ArrayList<Integer>();
    }

    public int add(int number){
            if(arr.isEmpty()){
                arr.add(number);
                return 0;
            }
            else {
                for (int i = 0; i < arr.size(); i++) {
                    if (arr.get(i) >= number) {
                        arr.add(i, number);
                        return i;
                    }
                }
                arr.add(number);
                return arr.size();
            }
    }

    public void remove(int index){
        arr.remove(index);
    }
}
