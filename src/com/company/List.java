package com.company;
import java.util.LinkedList;
import java.util.ListIterator;

public class List {
    LinkedList<Integer> list;

    public List() {
        list = new LinkedList();
    }

    public void add(int number){
        ListIterator<Integer> itr = list.listIterator();

         while(itr.hasNext()){
            if (itr.next() >= number) {
                itr.previous();
                itr.add(number);
                return;
            }
         }
         itr.add(number);
    }

    public void remove(int index){
        list.remove(index);
    }

}
